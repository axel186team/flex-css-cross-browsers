# The idea
----

Today we got few browsers and there is no standart for using `flex-boxes`.

Every browser uses his own methods: 
```css
    display: -webkit-box
    display: -moz-box
    display: -ms-flexbox
    display: -webkit-flex
    display: flex
```

... etc

---

# Solution

I added `stylus` mixins that mathces styles by browser.

## Example:

Lets add this to some class in stylus:

```styl
.my_div
    flex-container()
```

Ouput:

```css
.my_div{
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
}
```

# Next step

Need to convert mixins for other preprocessors: `sass/scss`, `less`.
